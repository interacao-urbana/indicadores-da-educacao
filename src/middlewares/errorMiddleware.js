const httpStatus = require('http-status-codes');

function statusCodeAsText(status) {
  return httpStatus.getStatusText(status);
}

module.exports = function (
  error,
  _req,
  res,
  next,
) {
  if (error) {
    console.error({ error });
    
    if(error.code && error.code === 'ETIMEDOUT') {
      res.status(httpStatus.GATEWAY_TIMEOUT).json({
        code: statusCodeAsText(httpStatus.GATEWAY_TIMEOUT),
        message: `the server exceed the time limit [${error.timeout / 1000}secs]`,
      });
      return;
    }

    const errorCode = httpStatus.INTERNAL_SERVER_ERROR;
    res.status(errorCode).json({
      code: statusCodeAsText(errorCode),
      message: 'Something went wrong',
    });

    return;
  }

  next();
}
