# INEP

## Installation

Instale node e o gerenciador de pacotes yarn, rode yarn install na raiz do projeto.

## Usage 

### Iniciar Server na porta 3000

```bash
node index.js
```

### Request no navegador
[http://localhost:3000/inep?UF=MINAS%20GERAIS&CITY=BELO%20HORIZONTE](http://localhost:3000/inep?UF=MINAS%20GERAIS&CITY=BELO%20HORIZONTE)

## Output

os dados estão no arquivo output.json

### Runnning with docker

1. build the image

```bash
docker build -t indic-educ:lastest .
```

2. Running the image

```bash
docker run --name indic-educ -p 3000:3000 indic-educ
```

3. access the [endpoint](http://localhost:3000/inep?UF=MINAS%20GERAIS&CITY=BELO%20HORIZONTE)

4. access the data without forcing no cache [endpoint](http://localhost:3000/inep?UF=MINAS%20GERAIS&CITY=BELO%20HORIZONTE&no_cache=true)

## deploy no heroku

1. logar no registry

```bash
heroku container:login
```

2. criar as variaveis de ambiente no painel do heroku conforme o arquivo `.env.example`

informar as credenciais para autenticar

3. subir a imagem para o registry do heroku

```bash
heroku container:push web --app nomeDoAppNoHeroku
```

4. subir a aplicação

```bash
heroku container:release web --app nomeDoAppNoHeroku
```

5. ver os logs da aplicação

```bash
heroku logs --tail --app nomeDoAppNoHeroku
```
