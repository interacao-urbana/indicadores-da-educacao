const { InepScrapperService, CacheService } = require('../services');

module.exports = {
    async getInepData(req, res) {
        const { UF: uf, PERIODO: periodo, CITY: city, no_cache } = req.query;
        const cacheKey = `${uf}_${city}${periodo ? `_${periodo}` : ''}`;
        console.log({ INFO: `Checking key [${cacheKey}] exists on cache` });
        const cachedData = CacheService.get(cacheKey);
        if(cachedData && !no_cache) {
            console.log({ INFO: `Returning data of the key [${cacheKey}] from the cache` });
            res.json({ info: JSON.parse(cachedData) });
            return;
        }
        
        const inepPageData = await InepScrapperService.getDataFromPage(uf, city, periodo);
        const normalizedInepData = InepScrapperService.normalizeData(inepPageData, periodo);
        console.log({ INFO: `Setting key [${cacheKey}] on cache` });
        CacheService.set(cacheKey, JSON.stringify(normalizedInepData));
        res.json({ info: normalizedInepData });
    }
};