const puppeteer = require('puppeteer');

const elementsSelector = require('./elementsSelector');
const config = require('../../config');

const { delay } = require('../../utils');

module.exports = async function get_data(uf, city, periodo) {
    console.log({ INFO: `Running on ${process.env.NODE_ENV} env.` });
    const browser = process.env.NODE_ENV === 'production' ? await puppeteer.launch({
        executablePath: config.executableChromiumPath,
        headless: true,
        args: [
            "--disable-gpu",
            '--no-sandbox',
            '--disable-setuid-sandbox'
        ],
    }) : await puppeteer.launch({ headless: false });

    const page = await browser.newPage();
    await page.goto(config.INEP_DATA_URL);
    await page.waitForNavigation();
    console.log({ INFO: `Entering on ${page.url()}` });

    /* PREENCHER DADOS */

    console.log({ INFO: 'clicking on first dropdown' });
    await page.$eval(elementsSelector.FIRST_DROPDOWN_SELECTOR, el => el.click());

    const ufSelector = elementsSelector.getFDropDownElementUfByTitle(uf);
    
    console.log({ INFO: `Waiting for the option UF: [${uf}]` });
    await page.waitForSelector(ufSelector, { timeout: 3000 });
    
    console.log({ INFO: `Selecting the item on first dropdown UF: [${uf}]` });
    await page.$eval(ufSelector, el => el.click());
    
    console.log({ INFO: `Waiting for 500ms` });
    await delay(500);
    
    // #CITY * 1- click dropdown, 2- click item
    console.log({ INFO: 'clicking on second dropdown' });
    await page.evaluate(() => {
        const [, secondDropdown] = document.querySelectorAll('.promptDropDownButton');
        secondDropdown.click();
    });
    
    
    console.log({ INFO: `Waiting for 600ms` });
    await delay(600);
    
    const citySelector = elementsSelector.getFDropDownElementCityByTitle(city);
    
    console.log({ INFO: `Waiting for the option CITY: [${city}]` });
    await page.waitForSelector(citySelector, { timeout: 3000 });

    console.log({ INFO: `Selecting the item on second dropdown CITY: ${city}` });
    await page.$eval(citySelector, el => el.click());

    console.log({ INFO: `Waiting for 500ms` });
    await delay(500);

    console.log({ INFO: `Get the link of results` });
    const [link] = await page.$x(elementsSelector.LINK_RESULTS);

    if (link) {
        await link.click();
    }

    await page.waitForNavigation();
    console.log({ INFO: `Entering on ${page.url()}` });


    if (periodo === 'anos-finais') {
        /*
        * Anos Finais
        */
        console.log('Clicking on anos finais items');
        await page.$eval(elementsSelector.ANOS_FINAIS_MENU_ITEM, item => item.click());

        console.log({ INFO: `Waiting for 1500ms` });
        await delay(1500);

        console.log({ INFO: `Entering on ${page.url()}` });
    } else {
        console.log({ INFO: 'Getting anos iniciais' });
    }

    /* RASPAR DADOS */
    const data = await page.evaluate(() => Array.from(document.querySelectorAll('.PTChildPivotTable table tr td')).map(el => el.innerText));

    console.log({ INFO: 'Closing browser' });
    await browser.close();

    const cityToValidate = data[0].length === 2 ? data[1] : data[0];

    if(city !== cityToValidate) {
        const correctData = await get_data(uf, city, periodo);
        return correctData;
    }
    
    return data;
}
