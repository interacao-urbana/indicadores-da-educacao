const CacheService = require('./CacheService');
const InepScrapperService = require('./InepScrapperService');

module.exports = {
    InepScrapperService,
    CacheService,
}