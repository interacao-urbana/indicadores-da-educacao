module.exports = {
    FIRST_DROPDOWN_SELECTOR: 'img[src="/analyticsRes/res/s_InepdataPainelMunicipal/master/selectdropdown_ena.png"]',
    LINK_RESULTS: "//a[contains(., 'Exibir Resultados')]",
    ANOS_FINAIS_MENU_ITEM: '#cssmenu_emec > ul > li > ul > li:nth-child(2) > a',
    getFDropDownElementUfByTitle(uf) {
        return `div[title="${uf}"]`;
    },
    getFDropDownElementCityByTitle(city) {
        return `div.promptMenuOption[title="${city}"]`
    }
}