const homeRoutes = require('./home');
const inepRoutes = require('./inep');

module.exports = {
    homeRoutes,
    inepRoutes,
};