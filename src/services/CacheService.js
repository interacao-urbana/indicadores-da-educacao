const NodeCache = require('node-cache');

const config = require('../config');

class CacheService {
    constructor(cacheProvider = new NodeCache(config.cacheProvider)) {
        this.cacheProvider = cacheProvider;
    }

    set(key, value) {
        this.cacheProvider.set(key, value);
    }

    get(key) {
        return this.cacheProvider.get(key);
    }
}

module.exports = new CacheService();