module.exports = {
    port: process.env.PORT || 3000,
    APPLICATION_TIMEOUT: process.env.APPLICATION_TIMEOUT || '60s',
    INEP_DATA_URL: process.env.INEP_DATA_URL || 'https://inepdata.inep.gov.br/analytics/saw.dll?Dashboard&NQUser=inepdata&NQPassword=Inep2014&PortalPath=%2Fshared%2FPainel%20Educacional%2F_portal%2FPainel%20Municipal',
    cacheProvider: { stdTTL: 3600, checkperiod: 120 },
    executableChromiumPath: process.env.CHROMIUM_PATH,
};