const express = require('express');
require('express-async-errors');
const morgan = require('morgan');
const cors = require('cors');
const timeout = require('connect-timeout');

const config = require('./config');
const routes = require('./routes');
const errorMiddleware = require('./middlewares/errorMiddleware');

class App {
    constructor() {
        this.server = express();
    }

    setupMiddlewares() {
        this.server.use(morgan('dev'));
        this.server.use(express.json());
        this.server.use(cors());
        this.server.use(timeout(config.APPLICATION_TIMEOUT));
    }

    setupGlobalErrorMiddleware() {
        this.server.use(errorMiddleware);
    }

    setupRoutes() {
        const routesList = Object.values(routes);
        routesList.forEach(route => this.server.use(route));
    }

    setupApplication() {
        this.setupMiddlewares();
        this.setupRoutes();
        this.setupGlobalErrorMiddleware();
    }

    initApplication() {
        const PORT = config.port;
        this.setupApplication();
        this.server.listen(PORT, () => console.log(`Express started at http://localhost:${PORT}`));
    }
}

module.exports =new App();