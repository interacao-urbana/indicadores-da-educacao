const getDataFromPage = require('./getDataFromPage');
const normalizeData = require('./normalizeData');

module.exports = {
    normalizeData,
    getDataFromPage,
};