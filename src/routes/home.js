const { Router } = require('express');

const routes = Router();

routes.get('/', (_req, res) => {
    res.send('Hello World', 200);    
});  

module.exports = routes;