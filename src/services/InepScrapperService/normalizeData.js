module.exports = (data, periodo) => {
    const has_rm = (data[8] === 'Rede Municipal (RM)') ? true : false;
    
    let has_rem = (data[8] === 'Rede Estadual situada no seu município (REM)') ? true : false;
    if (has_rm) {
        has_rem = (data[11] === 'Rede Estadual situada no seu município (REM)') ? true : false;
    }
    
    let info;

    if (has_rm && has_rem) {
        info = {
            'Cidade': data[0].length === 2 ? data[1] : data[0],
            'Estado': data[0].length === 2 ? data[0] : data[1],
            'Ano': data[21],
            'REDES': [{
                'Rede': data[8], //'Rede Municipal (RM)'
                'Escolas': data[9],
                'Matrículas': data[10]
            },
            {
                'Rede': data[11], //'Rede Estadual situada no seu município (REM)'
                'Escolas': data[12],
                'Matrículas': data[13]
            }],
            'INDICADORES': []
        };
    } else {
        info = {
            'Cidade': data[0].length === 2 ? data[1] : data[0],
            'Estado': data[0].length === 2 ? data[0] : data[1],
            'Ano': data[18],
            'REDES': [{
                'Rede': data[8],
                'Escolas': data[9],
                'Matrículas': data[10]
            }],
            'INDICADORES': []
        };
    }

    const posini = (has_rm && has_rem) ? 16 : 13;

    const SECTION = [
        'Matrículas',
        'Total de Estudantes Incluídos',
        'Taxa de Aprovação (%)',
        'Taxa de Abandono (%)',
        'Média Estudantes por Turma',
        'Matrículas em Tempo Integral',
        'Taxa de Reprovação (%)',
        'Taxa de Distorção Idade-série (%)'
    ];

    for (i = posini; i <= data.length; i++) {
        let position = (has_rm && has_rem) ? 24 : 18;

        for (let sect = 0; sect <= SECTION.length; sect++) {
            if (data[i] == SECTION[sect] && SECTION[sect] !== undefined) {
                let year_start = (periodo === 'anos-finais') ? 6 : 1;
                let year_end = (periodo === 'anos-finais') ? 9 : 5;

                const ind = {
                    "Indicador": data[i],
                    "ANOSESCOLARES": []
                }

                const len = info.INDICADORES.push(ind);

                for (year = year_start; year <= year_end; year++) {

                    if (has_rm && has_rem) {
                        var indanos = {
                            'AnoEscolar': year,
                            'ANOS': [{
                                'ANO': data[i + 3],
                                'RM': data[i + position],
                                'REM': data[i + position + 1]
                            },
                            {
                                'ANO': data[i + 4],
                                'RM': data[i + position + 2],
                                'REM': data[i + position + 3]
                            },
                            {
                                'ANO': data[i + 5],
                                'RM': data[i + position + 4],
                                'REM': data[i + position + 5]
                            }]
                        };

                        position += 8;
                    } else {
                        if (has_rm) {
                            var indanos = {
                                'AnoEscolar': year,
                                'ANOS': [{
                                    'ANO': data[i + 3],
                                    'RM': data[i + position]
                                },
                                {
                                    'ANO': data[i + 4],
                                    'RM': data[i + position + 1]
                                },
                                {
                                    'ANO': data[i + 5],
                                    'RM': data[i + position + 2]
                                }
                                ]
                            };
                            }
                            else {
                                var indanos = {
                                    'AnoEscolar': year,
                                    'ANOS': [{
                                        'ANO': data[i + 3],
                                        'REM': data[i + position]
                                    },
                                    {
                                        'ANO': data[i + 4],
                                        'REM': data[i + position + 1]
                                    },
                                    {
                                        'ANO': data[i + 5],
                                        'REM': data[i + position + 2]
                                    }
                                    ]
                                };
                                    }
                        position += 5;
                    }

                    info.INDICADORES[len - 1].ANOSESCOLARES.push(indanos);
                }
            }
        }
    }

    return info;
}
