const { Router } = require('express');

const { inepController } = require('../controllers');

const routes = Router();

routes.get(`/inep`, inepController.getInepData);

module.exports = routes;